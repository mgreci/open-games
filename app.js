const http = require("http");
const app = require("express")();
const fs = require("fs").promises;

const GAME_HISTORY_FILE = "./game_history.json";

const PORT = process.env.PORT || 3001;

const server = http.createServer(app);
const io = require("socket.io").listen(server);

server.listen(PORT);

const users = {
	list: [],
	add: user => {
		users.list.push(user);
		io.emit("users", users.list);
	},
	random: () => {
		return users.list[Math.floor(Math.random() * users.list.length)];
	}
};

const game = {
	status: "waiting",
	hangman: null,
	guessHistory: [],
	makedWord: null,
	guessesRemaining: 7,
	start: () => {
		game.status = "chooseWord";
		game.hangman = users.random();
		game.makedWord = null;
		game.guessHistory = [];
		game.guessesRemaining = 7;

		io.emit("game", game.status);
		io.emit("hangman", game.hangman);
		io.emit("maskedWord", game.maskedWord);
		io.emit("guessHistory", game.guessHistory);
		io.emit("guessesRemaining", game.guessesRemaining);
	},
	startGuessing: () => {
		game.status = "guessing";
		game.guessHistory = [];
		game.maskedWord = game.generateMaskedWord();
		io.emit("game", game.status);
		io.emit("maskedWord", game.maskedWord);
	},
	generateMaskedWord: () => {
		return game.secret.word.toUpperCase().split("").map(character => game.guessHistory.includes(character) || character === " " ? character : "*");
	},
	gameover: async (hangmanWins) => {
		game.status = "gameover";
		game.hangmanWins = hangmanWins;

		// read game history
		let gameHistory = await fs.readFile(GAME_HISTORY_FILE, { encoding: "utf8" });
		if (gameHistory === "") {
			gameHistory = {};
		} else {
			gameHistory = JSON.parse(gameHistory);
		}

		const connectedUsersHistory = {};

		// update winners/losers
		users.list.forEach(user => {
			if (!gameHistory[user]) {
				gameHistory[user] = {
					hangman: {
						wins: 0,
						loses: 0,
					},
					guessing: {
						wins: 0,
						loses: 0,
					},
				};
			}

			if (user === game.hangman) {
				// hangman record
				if (hangmanWins) {
					// increment wins
					gameHistory[user]["hangman"]["wins"] += 1;
				} else {
					// increment loses
					gameHistory[user]["hangman"]["loses"] += 1;
				}
			} else {
				// player record
				if (!hangmanWins) {
					// increment wins
					gameHistory[user]["guessing"]["wins"] += 1;
				} else {
					// increment loses
					gameHistory[user]["guessing"]["loses"] += 1;
				}
			}

			// capture history for connected players
			connectedUsersHistory[user] = gameHistory[user];
		});

		io.emit("game", game.status);
		io.emit("hangmanWins", game.hangmanWins);

		// emit only history to connected players
		io.emit("gameHistory", connectedUsersHistory);

		// save to disk
		fs.writeFile(GAME_HISTORY_FILE, JSON.stringify(gameHistory, null, 2))
		.catch(console.log);
	},
	updateRemainingGuesses: guess => {
		if (!game.secret.word.includes(guess)) {
			game.guessesRemaining--;
		}

		game.maskedWord = game.generateMaskedWord();

		// emit masked word
		io.emit("maskedWord", game.maskedWord);
	},
	determineGameStatus: () => {
		// if no remaining guesses left
		if (game.guessesRemaining <= 0) {
			game.gameover(true);
		} else if (!game.generateMaskedWord().includes("*")) {
			game.gameover(false);
		}
	},
	recordGuess: guess => {
		// if guess is single letter
		if (typeof guess === "string" && guess.length === 1) {
			guess = guess.toUpperCase();

			// if guess NOT already made
			if (!game.guessHistory.includes(guess)) {
				// add to history
				game.guessHistory.push(guess);

				// determine what client may see of word
				game.maskedWord = game.generateMaskedWord();

				// update remaining guesses
				game.updateRemainingGuesses(guess);

				// determine game over
				game.determineGameStatus();

				// emit list of guesses
				io.emit("guessHistory", game.guessHistory);

				// emit guesses remaining
				io.emit("guessesRemaining", game.guessesRemaining);
			}
		}
	},
	secret: {
		word: null
	}
};

io.on("connection", client => {
	io.emit("game", game.status);
	io.emit("users", users.list);
	io.emit("hangman", game.hangman);
	io.emit("maskedWord", game.maskedWord);
	io.emit("guessHistory", game.guessHistory);
	io.emit("guessesRemaining", game.guessesRemaining);

	client.on("register", name => {
		users.add(name);
	});

	client.on("start", start => {
		if (start) {
			// start game
			game.start();
		}
	});

	client.on("word", word => {
		// upper case secret word
		word = word.toUpperCase();

		game.secret.word = word;
		game.startGuessing();
	});

	client.on("guess", game.recordGuess);
});